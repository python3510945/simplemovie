from django.contrib.flatpages.views import flatpage
from django.db.models import Q, Avg, Subquery, OuterRef, IntegerField, Value
from django.db.models.functions import Cast, Coalesce
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView

from django.views.generic.base import View

from .models import Movie, Category, Actor, Genre, Rating
from .forms import ReviewForm, RatingForm


class GenreYear:

    def get_genres(self):
        return Genre.objects.all().order_by('name')

    def get_years(self):
        return Movie.objects.filter(draft=False).values('year').distinct().order_by('-year')


class About(View):
    def get(self, request):
        return flatpage(request, "/about/")


class MovieView(GenreYear, ListView):
    model = Movie
    queryset = Movie.objects.filter(draft=False).annotate(
        average_rating=Coalesce(
            Cast(
                Subquery(
                    Rating.objects.filter(movie=OuterRef('pk')).values('movie').annotate(
                        avg_rating=Avg('star__value')
                    ).values('avg_rating')[:1],
                ),
                IntegerField()
            ),
            Value(0)
        )
    )
    paginate_by = 6
    template_name = "movies/movies.html"

    # def get(self, request):
    #     movies = Movie.objects.all()
    #     return render(request, 'movies/movies.html', {"movie_list": movies})
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        # for movie in self.object_list:
        #     avg_rating = Rating.objects.filter(movie=movie).aggregate(Avg('star__value'))['star__value__avg']
        #     setattr(movie, 'average_rating', int(avg_rating) if avg_rating is not None else 0)
        context['stars_range'] = range(1, 6)
        context['categories'] = Category.objects.all()
        return context


class MovieDetailView(GenreYear, DetailView):
    model = Movie
    slug_field = 'url'
    template_name = "movies/movie_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        average_rating = Rating.objects.filter(movie=context['movie']).aggregate(Avg('star__value'))['star__value__avg']
        context["star_form"] = RatingForm()
        context["average_rating"] = f'{int(average_rating)}' if average_rating is not None else f'0'
        context["form"] = ReviewForm()
        return context

    # def get(self, request, slug):
    #     movie = Movie.objects.get(url=slug)
    #     return render(request, 'movies/movie_detail.html', {"movie": movie})


class AddReview(View):
    def post(self, request, pk):
        form = ReviewForm(request.POST)
        movie = Movie.objects.get(id=pk)
        if form.is_valid():
            form = form.save(commit=False)
            if request.POST.get('parent', None):
                form.parent_id = int(request.POST.get('parent'))
            form.movie = movie
            form.save()

        return redirect(movie.get_absolute_url())


class ActorView(GenreYear, DetailView):
    model = Actor
    template_name = 'movies/actor.html'
    slug_field = 'name'


class FilterMovieView(GenreYear, ListView):
    template_name = "movies/movies.html"
    paginate_by = 6

    def get_queryset(self):
        queryset = Movie.objects.filter(
            Q(year__in=self.request.GET.getlist('year')) |
            Q(genres__in=self.request.GET.getlist('genre'))
        ).distinct()

        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['year'] = ''.join([f"year={x}&" for x in self.request.GET.getlist('year')])
        context['genre'] = ''.join([f"genre={x}&" for x in self.request.GET.getlist('genre')])
        return context


class JsonFilterMoviesViews(ListView):
    def get_queryset(self):
        queryset = Movie.objects.filter(
            Q(year__in=self.request.GET.getlist('year')) |
            Q(genres__in=self.request.GET.getlist('genre')))
        return queryset

    def get(self, request, *args, **kwargs):
        queryset = list(self.get_queryset())
        return JsonResponse({"movies": queryset}, safe=False)


class AddStarRating(View):
    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.update_or_create(
                ip=self.get_client_ip(request),
                movie_id=int(request.POST.get('movie')),
                defaults={'star_id': int(request.POST.get('star'))}
            )
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)


class Search(ListView):
    template_name = "movies/movies.html"
    paginate_by = 6

    def get_queryset(self):
        return Movie.objects.filter(title__icontains=self.request.GET.get('q'))

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['q'] = f'q={self.request.GET.get("q")}&'
        return context
