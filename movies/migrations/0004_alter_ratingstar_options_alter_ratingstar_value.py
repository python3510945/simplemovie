# Generated by Django 4.2.5 on 2023-10-04 07:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0003_alter_movie_tagline'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ratingstar',
            options={'ordering': ['-value'], 'verbose_name': 'Зірка рейтингу', 'verbose_name_plural': 'Зірки рейтингу'},
        ),
        migrations.AlterField(
            model_name='ratingstar',
            name='value',
            field=models.SmallIntegerField(default=0, verbose_name='Значення'),
        ),
    ]
