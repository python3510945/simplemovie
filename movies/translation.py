# from django.utils.translation import gettext_lazy as _
# from modeltranslation.templatetags.modeltranslation_tags import register
# from modeltranslation.translator import TranslationOptions
#
# from .models import Category, Actor, Movie, Genre, MovieShots
#
#
# @register(Category)
# class CategoryTranslationOptions(TranslationOptions):
#     fields = ('name', 'description')
#
#
# @register(Actor)
# class ActorTranslationOptions(TranslationOptions):
#     fields = ('name', 'description')
#
#
# @register(Genre)
# class GenreTranslationOptions(TranslationOptions):
#     fields = ('name', 'description')
#
#
# @register(Movie)
# class MovieTranslationOptions(TranslationOptions):
#     fields = ('title', 'tagline', 'description', 'country')
#
#
# @register(MovieShots)
# class MovieShotsTranslationOptions(TranslationOptions):
#     fields = ('title', 'description')